package com.ds.util.adblib.adb;

import com.blankj.utilcode.util.ShellUtils;
import com.blankj.utilcode.util.Utils;

public class BlankjAdbShell implements IAdb{

    @Override
    public ShellUtils.CommandResult runCmd(String command, boolean isRooted) {
        return ShellUtils.execCmd(command, isRooted);
    }

    @Override
    public ShellUtils.CommandResult runCmd(String[] command, boolean isRooted) {
        return ShellUtils.execCmd(command, isRooted);
    }
}
