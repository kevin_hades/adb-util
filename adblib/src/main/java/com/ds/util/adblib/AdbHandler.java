package com.ds.util.adblib;

import android.util.Log;

import com.ds.util.adblib.adb.BlankjAdbShell;
import com.ds.util.adblib.adb.IAdb;
import com.ds.util.adblib.result.CommandResponse;

import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class AdbHandler {

    private AdbHandler() {
        adbExecutor = new BlankjAdbShell();
    }

    private static class SingletonInstance {
        private static final AdbHandler INSTANCE = new AdbHandler();
    }

    public static AdbHandler getInstance() {
        return SingletonInstance.INSTANCE;
    }
    private String splitRegex = "\\r?\\n";
    private IAdb adbExecutor;

    ThreadPoolExecutor executor = new ThreadPoolExecutor(1, //始终存在一个线程
            1, // 核心线程满后可创建最大线程数
            30,// 超时回收线程
            TimeUnit.SECONDS,
            new LinkedBlockingDeque<Runnable>(2),
            new ThreadPoolExecutor.DiscardOldestPolicy());// 丢弃策略，丢弃早先的任务


    public void setSplitRegex(String regex){
        this.splitRegex = regex;
    }

    public void executeAdb(String adbString, boolean su, ADBResult result){
        AsynchronousMultiAdb(adbString, su, result);
    }


    public void AsynchronousMultiAdb(String adbStrings, boolean su, ADBResult resultCallBack) {
            CommandResponse result = new CommandResponse(-1, "", "");
            String[] temp = cookAdbCommons(adbStrings);
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    Log.d("AdbHandler", "Current Thread = "+ Thread.currentThread().getName());
                    try {
                        if (temp.length<=0){
                            resultCallBack.onAdbResult(new CommandResponse(-1, "", ""));
                        }else {
                            resultCallBack.onAdbResult(initTaskList(temp, su));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        resultCallBack.onAdbResult(result);
                    }
                }
            });
        executor.execute(thread);
    }

    private String[] cookAdbCommons(String adbStrings) {
        String[] temp;
        try {
            temp = adbStrings.split(splitRegex);
        } catch (Exception e) {
            e.printStackTrace();
            temp = new String[]{};
        }
        return temp;
    }


    private CommandResponse initTaskList(String[] temp, boolean su) {
        return new CommandResponse(adbExecutor.runCmd(temp, su));
    }


}