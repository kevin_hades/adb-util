package com.ds.util.adblib.result;

import android.os.SystemClock;

import com.blankj.utilcode.util.ShellUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CommandResponse {
    public int    result;
    public String time;
    public String successMsg;
    public String errorMsg;

    public CommandResponse(int result, String successMsg, String errorMsg) {
        this.result = result;
        this.successMsg = successMsg;
        this.errorMsg = errorMsg;
        this.time = stampToDate(String.valueOf(System.currentTimeMillis()));
    }

    public CommandResponse(ShellUtils.CommandResult r) {
        this.result = r.result;
        this.successMsg = r.successMsg;
        this.errorMsg = r.errorMsg;
        this.time = stampToDate(String.valueOf(System.currentTimeMillis()));
    }

    public void copyFrom(ShellUtils.CommandResult r){
        this.result = r.result;
        this.successMsg = r.successMsg;
        this.errorMsg = r.errorMsg;
        this.time = stampToDate(String.valueOf(System.currentTimeMillis()));
    }

    @Override
    public String toString() {
        return "result: " + result + "\n" +
                "time: " + time + "\n" +
                "successMsg: " + successMsg + "\n" +
                "errorMsg: " + errorMsg;
    }

    /*
     * 将时间戳转换为时间
     *
     * s就是时间戳
     */

    public static String stampToDate(String s) {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //如果它本来就是long类型的,则不用写这一步
        long lt = new Long(s);
        Date date = new Date(lt);
        res = simpleDateFormat.format(date);
        return res;
    }
}
