package com.ds.util.adblib;

import com.blankj.utilcode.util.ShellUtils;
import com.ds.util.adblib.result.CommandResponse;

public interface ADBResult {
    void onAdbResult(CommandResponse result);
}
